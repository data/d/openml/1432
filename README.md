# OpenML dataset: colon-cancer

https://www.openml.org/d/1432

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: U. Alon","N. Barkai","D. A. Notterman","K. Gish","S.Ybarra","D.Mack","and A. J. Levine.  
libSVM","AAD group  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html) - Date unknown  
**Please cite**: U. Alon, N. Barkai, D. A. Notterman, K. Gish, S.Ybarra, D.Mack, and A. J. Levine.
Broad patterns of gene expression revealed by clustering analysis of tumor and normal colon tissues probed by oligonucleotide arrays.
Cell Biology, 96:6745-6750, 1999.  

#Dataset from the LIBSVM data repository

Preprocessing: Instance-wise normalization to mean zero and variance one. Then feature-wise normalization to mean zero and variance one

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1432) of an [OpenML dataset](https://www.openml.org/d/1432). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1432/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1432/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1432/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

